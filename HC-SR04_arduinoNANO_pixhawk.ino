 /*************************************************************************
 * 
 * Alexander Buckley <albuckley@gmail.com>
 * Aug 2015
 * __________________
 * 
 * HC-SR04_arduinoNANO_pixhawk.ino
 * v1.0
 *  
 * Arduino Nano sketch to drive the HC-SR04 sonar module.
 * Outputs PWM signal representing distance (5v peak).
 * Intended to integrate the HC-SR04 with the Pixhawk Flight Control Board.
 *
 * __________________
 * Requirements:
 *
 * The Pixhawk expects to see an analog voltage (MAX 3.3v on ADC IN), 
 * the Nano analog output pins are not true analog, they are PWM.
 * A conversion circuit is required to both step down the voltage and 
 * to convert the signal to a near analog DC signal.
 * I.E.: Voltage divider and low pass filter.
 *
 *	Nano 5v PWM out  o---#######---------------o  Pixhawk 3.3v DC in
 *	                       R1      |     |
 *	                               #     |
 *	                               #    ---
 *	                           R2  #    --- C1
 *                                 #     |
 *	                               |     |  
 *             GND   o--------------------------o   GND
 *
 *  R1	5.6k ohm
 *  R2	10k	 ohm
 *  C1	33uF  
 * 	
 *	I have some slight noise received by pixhawk (negligible for now). 
 *	I have not ruled out unshielded cabling as the cause given the noisy environment.
 *	A little more capacitance may help.
 *
 *	Given component tolerances, test and ensure that no more than 3.3v is output with max signal.
 * (Uncomment provided test output code)
 *
 *  The Pixhawk does provide 3.3v on the ADC3.3 port, the Nano can be powered with 3.3v 
 *   though it cannot drop below 3.3v.
 *	I chose to power the Nano with 12v taken from the Pixhawk servo rail to be sure.
 *	You will want to determine the power consumption of the Nano and SR04 otherwise
 * 	and compare with the Pixhawk ADC3.3 supply limit.
 *
 *
 * 	When the cicuit is completed and integrated, there will be some variance from 
 *	one implementation to another given component tolerances and circuit/connector losses.
 *  Find and adjust RNGFND_GAIN in your ground station config settings and adjust as nessesary.
 *	Attach USB cable to nano, uncomment distance cm output, and compare readings to those reported
 *  by the ground station data viewer.
 *
 * 	This driver could be expanded quite simply to control 2 sonar modules.
 *
 *	The most challenging requirement is most likely sourcing the 5 pin DF13 connector
 *	 that is needed on the pixhawk.
 */

 /* ============================================

Copyright (c) 2015 Alexander Buckley (ALX)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
===============================================*/


#define MIN_DUTY_CYCLE	0
#define MAX_DUTY_CYCLE 	255	// max output voltage 5v then filtered to 3.3v
#define MIN_DIST	0
#define MAX_DIST	400	// cm

int vcc 	= 7; //attach pin 2 to vcc
int trig 	= 6; // attach pin 3 to Trig
int echo 	= 5; //attach pin 4 to Echo
int gnd 	= 4; //attach pin 5 to GND

int PWM	= 3; // output voltage sent to pixhawk ADC 3.3v connector

int distance;

int debug_value;

void setup() {

pinMode (vcc,OUTPUT);
pinMode (gnd,OUTPUT);
pinMode (trig, OUTPUT);
pinMode (echo,INPUT);
pinMode (PWM, OUTPUT);

// initialize serial communication:
Serial.begin(9600);
}

void loop()
{
digitalWrite(vcc, HIGH);
// establish variables for duration of the ping,
// and the distance result in centimeters:
long duration, inches, cm;

// The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
// Give a short LOW pulse beforehand to ensure a clean HIGH pulse:

digitalWrite(trig, LOW);
delayMicroseconds(2);
digitalWrite(trig, HIGH);
delayMicroseconds(5);
digitalWrite(trig, LOW);

// The same pin is used to read the signal from the PING: a HIGH
// pulse whose duration is the time (in microseconds) from the sending
// of the ping to the reception of its echo off of an object.
duration = pulseIn(echo, HIGH);

// convert the time into a distance
//inches = microsecondsToInches(duration);
cm = microsecondsToCentimeters(duration);

// map distance range to duty cycle range
distance = map(cm, MIN_DIST, MAX_DIST, MIN_DUTY_CYCLE, MAX_DUTY_CYCLE);

// output PWM signal
analogWrite(PWM, distance); // where dutyCycle is a value from 0 to 255,



/* TEST CODE  */
// This will output maximum value.
// Make sure your converstion circuit outputs no more than 3.3v with multimeter.
// Adjust resitance if nessesary.
//analogWrite(PWM, 255);  



/* some debug code for testing */
/*
debug_value = map(cm, MIN_DIST, MAX_DIST, 0, 178); // screen width in chars
Serial.print(cm);
for(int i = 0; i < debug_value; i++)
{
	Serial.print('_');
}
Serial.println();
*/

// OR

/*
Serial.print(cm);
Serial.print("cm");
Serial.println();
*/


// loop timing
delay(100);
}


long microsecondsToCentimeters(long microseconds)
{
	// The speed of sound is 340 m/s or 29 microseconds per centimeter.
	// The ping travels out and back, so to find the distance of the
	// object we take half of the distance travelled.
	return microseconds / 29 / 2;
}
